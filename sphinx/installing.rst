************
Installation
************

There are several different methods to get up and running with Shyft depending on your
intended use.
We categorize users into several categories:

- **users** are those interested in using Shyft for time-series, dashboard, energy-market or hydrologic analysis.
- **contributers** are those who will explore the Python api and may contribute back to the project.
- **developers** are those who are interested in the C++ core, and are interested in creating their own algorithms. Advance programming skills and familiarity is required.

The reason to categorize users relates to the installation requirements. For **users**
it is not necessary to build/compile the C++ core, and one can simply use available
distributions. **Contributors** also don't need to build the C++ core, but will want
to clone the repository to have access to the Python source code -- mostly for making
modifications to orchestration.

Shyft is developed for both Unix-like and Windows operating systems, though we
have a strong preference for linux, and recommend that as preferred platform for all serious work.

The only reason we are using conda/anaconda is due to the windows python package support, that allows us to
safely provide reasonable consistent python environment on the windows-platform, as long as it is relevant.

With small adjustments it is possible to build and develop Shyft on a decent updated linux distro,
using the distro package manager to pull in the required dependencies, and maybe just build locally those dependencies that
are outdated on the distro (arch-linux is  always up to date, and needs no local builds).

Refer to: `shyft wiki <https://gitlab.com/shyft-os/shyft/wikis/home>`_

for updated documentation about installing as **users**, installing as for python **contributers**,
for installing as full stack **developers**.

Linux
-----

To setup an Ubuntu desktop for development you can follow this link:

`Ubuntu dev-desktop complete <https://gitlab.com/shyft-os/shyft/wikis/How-to-setup-complete-dev-desktop-for-ubuntu-19.04>`_

Windows
-------
To work with windows (if thats your only option):

`Windows dev-desktop complete <https://gitlab.com/shyft-os/shyft/wikis/BuildCplusplus#c-buildinstall>`_
