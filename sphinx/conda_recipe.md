# Introduction

In the platform specific subsections you find how to install the standard Python environment needed to run Shyft.

## conda environment
A pre-built shyft-distro is available on anaconda channel shyft-os, https://anaconda.org/shyft-os, and the shyft wiki
 descibes how to setup a development environment, linux (even windows), - as well as gitlab-runners.

## pypi, pip environment

Use pip install shyft, shyft.time-series or shyft.dashboard to install your preferred composition.

## more info

Ref to [shyft-wiki](https://gitlab.com/shyft-os/shyft/wikis/home) for details and updated docs.
